# Taller6-MISO4208

**Link a la página**: https://afdonoso.gitlab.io/taller6-miso4208

**Link al repo con la herramienta de automatización:** https://gitlab.com/afDonoso/taller6-miso4208-autovrt

## Cypress: Screenshots

Al hacer los screenshots con Cypress, podemos notar que este toma un screenshot de toda la pantalla. Es decir, entre más grande sea la página (moverse hacia abajo, o hacia los lados) mayor tiempo le va a tomar a las pruebas de VRT completarse, dado que éstas tienen que analizar todo el screenshot. Así, a medida que aumenta el tamaño de la imagen, aumenta el tiempo requerido para realizar la prueba.

![Screenshot antes](images/Login%20Tests%20--%20makes%20a%20right%20login%20attemp.png)

![Screenshot despues](images/Login%20Tests%20--%20makes%20a%20wrong%20login%20attemp.png)

## ResembleJS

**¿Qué información puedo obtener de una imagen al usar resembleJS y que significado tiene cada uno de los componentes de la respuesta?**

De una imagen, podemos obtener información como el RGBA, B/W y el brillo de ésta. Al hacer una análisis básico sobre la imagen, sólo estamos viendo cuáles son los colores que se están utilizando en la foto.

**¿Qué información puedo obtener al comparar dos imagenes?**

Al comparar dos imágenes, podemos obtener las diferencias entre éstas. ResembleJS permite utilizar una variedad de opciones para comparar diferentes aspectos de la imagen, así como ignorar otros. Esto produce una imagen resaltando las diferencias y un porcentaje que indica qué tan diferentes son.

**¿Qué opciones se pueden seleccionar al realizar la comparación?**

- Filtros
  - Ignore nothing
  - Ignore less
  - Ignore colors
  - Ignore antialiasing
  - Ignore alpha
- Scaling
  - Original size
  - Scale to same size
- Error types
  - Flat
  - Movement
  - Flat with diff intensity
  - Movemint with diff intensity
  - Diff portion from input

## Back-End herramienta de automatización

![Video función](images/Funcionamiento%20back.gif)
