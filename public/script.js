const numColors = 5
const tonalDifference = 360 / numColors

function randomPalette() {
  console.log('Method RUnned.')
  let colors = generateColors()

  //Elements to be changed
  let color1 = document.getElementById('color1')
  let color2 = document.getElementById('color2')
  let color3 = document.getElementById('color3')
  let color4 = document.getElementById('color4')
  let color5 = document.getElementById('color5')

  for (let i = 0; i < colors.length; i++) {
    let rgb = hslToRgb(colors[i] / 359, 1, 0.5)
    let rgbFormatted = `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`

    switch (i) {
      case 0:
        color1.style.color = rgbFormatted
        break

      case 1:
        color2.style.color = rgbFormatted
        break

      case 2:
        color3.style.borderColor = rgbFormatted
        break

      case 3:
        color4.style.backgroundColor = rgbFormatted
        break

      case 4:
        color5.style.color = rgbFormatted
        break
    }
  }

  generateRules(colors)
}

/**
 *
 * @param {Array} colors Array of color hues
 */
function generateRules(colors) {
  let textArea = document.getElementById('css-rules')
  let message = ''

  for (let i = 0; i < colors.length; i++) {
    let rgb = hslToRgb(colors[i] / 359, 1, 0.5)
    let rgbFormatted = `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`

    switch (i) {
      case 0:
        message += `.website-background{ color: ${rgbFormatted};}\n\n`
        break

      case 1:
        message += `.element-text{ color: ${rgbFormatted};}\n\n`
        break

      case 2:
        message += `.element-border{ border-color: ${rgbFormatted};}\n\n`
        break

      case 3:
        message += `.element-background{ background-color: ${rgbFormatted};}\n\n`
        break

      case 4:
        message += `.header{ color: ${rgbFormatted};}\n\n`
        break
    }
  }

  textArea.innerText = message
}

function cleanPalette() {
  //Elements to be changed
  let color1 = document.getElementById('color1')
  let color2 = document.getElementById('color2')
  let color3 = document.getElementById('color3')
  let color4 = document.getElementById('color4')
  let color5 = document.getElementById('color5')
  let textArea = document.getElementById('css-rules')

  let message = ''

  for (let i = 0; i < 5; i++) {
    switch (i) {
      case 0:
        color1.style.color = 'black'
        message += `.website-background{ color: #FFF;}\n\n`
        break

      case 1:
        color2.style.color = 'black'
        message += `.element-text{ color: #FFF;}\n\n`
        break

      case 2:
        color3.style.borderColor = 'black'
        message += `.element-border{ border-color: #FFF;}\n\n`
        break

      case 3:
        color4.style.backgroundColor = 'white'
        message += `.element-background{ background-color: #FFF;}\n\n`
        break

      case 4:
        color5.style.color = 'black'
        message += `.header{ color: #FFF;}\n\n`
        break
    }
  }

  textArea.innerText = message
}

/**
 * @returns {Array} Array containing the 5 color's hues.
 */
function generateColors() {
  const initialHue = Math.floor(Math.random() * 360)
  console.log(initialHue)
  let colors = []

  for (let i = 0; i < numColors; i++) {
    let colorHue = initialHue + tonalDifference * i

    if (colorHue > 359) {
      colorHue -= 360
    }

    colors.push(colorHue)
  }

  return colors
}
